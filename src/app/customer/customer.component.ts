import { Component, OnInit, Input } from '@angular/core';
import { AppService } from '../services/app.service';
import { HttpClient } from "@angular/common/http";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MessageService } from 'primeng/api';
import { Router } from '@angular/router';

@Component({
  selector: 'app-customer',
  providers: [HttpClient, MessageService],
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss']
})
export class CustomerComponent implements OnInit {
  customerForm: FormGroup;
  customerCreditAmountForm: FormGroup;
  customers: any = [];
  mockCustomers: any = [];
  loading: boolean = false;
  constructor(private formBuilder: FormBuilder, private router: Router, private appService: AppService, private messageService: MessageService) { }

  ngOnInit() {
    this.registerFormBuilder();
    this.getCustomers();
  }

  registerFormBuilder() {
    this.customerCreditAmountForm = this.formBuilder.group({
      custId: ['', Validators.required],
      amount: ['', Validators.required]
    });

    this.customerForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      emailId: ['', Validators.compose([Validators.required, Validators.email])],
    });
  }
  getCustomers() {
    this.appService.getCustomers().subscribe(data => {
      console.log("Customers : ", data);
      this.customers = data;
    });
  }

  onSubmit() {
    console.log("Submiitted", this.customerForm.value);
    this.appService.addCustomer(this.customerForm.value).subscribe(res => {
      this.messageService.add({ severity: 'success', summary: 'Service Message', detail: 'Customer Successfully Added' });
      this.loading = false;
      console.log("Customers Added : ", res);
      this.customerForm.reset();
      this.getCustomers();
      this.router.navigate(['']);
    }, err => {
      console.log("err", err);
      this.loading = false;
      this.messageService.add({ severity: 'error', summary: 'Error', detail: 'There is some issue while adding customer' });
    });
  }

  initiateCredit(){
    this.appService.creditAmount(this.customerCreditAmountForm.value.custId,this.customerCreditAmountForm.value.amount).subscribe(res => {
      this.messageService.add({ severity: 'success', summary: 'Service Message', detail: 'Amount Creditted Successfully' });
      this.loading = false;
      console.log("Customers Added : ", res);
      this.customerCreditAmountForm.reset();
      this.getCustomers();
      this.router.navigate(['']);
    }, err => {
      console.log("err", err);
      this.loading = false;
      this.customerCreditAmountForm.reset();
      //this.messageService.add({ severity: 'error', summary: 'Error', detail: 'There is some issue while adding customer' });
      this.router.navigate(['']);
    });
  }

}
