import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class AppService {

  
  constructor(private http: HttpClient) { }

  getCustomers() {
    return this.http.get(environment.BASE_URL + 'customers');
  }

  addCustomer(payload) {
    return this.http.post(environment.BASE_URL + 'customer/add',payload);
  }

  creditAmount(customerId,amount) {
    return this.http.post(environment.BASE_URL + 'customer/initialcredit/' + customerId + '/' + amount, '');
  }
}
